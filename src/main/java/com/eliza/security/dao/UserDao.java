package com.eliza.security.dao;

import com.eliza.security.model.Permission;
import com.eliza.security.model.UserModel;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserDao {
    List<UserModel> getUserByName(String userName);

    List<Permission> getPermissionByUserId(Integer id);
}
