package com.eliza.security.service;

import com.eliza.security.fronts.LoginModel;
import com.eliza.security.model.Permission;
import com.eliza.security.model.UserModel;

import java.util.List;

public interface UserService {
    UserModel getUserByName(String userName);

    List<Permission> getPermissionByUserId(Integer id);

    String login(LoginModel loginModel);
}
