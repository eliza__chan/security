package com.eliza.security.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Date;
import java.util.Set;

@Data //自动设置方法
@AllArgsConstructor //有参构造器
@NoArgsConstructor//无参构造器
public class UserModel implements UserDetails {
    private Integer id;
    private String userName;//用户名
    private String password;//密码
    private String icon;//头像
    private String email;//邮箱
    private String nickName;//昵称
    private String note;//备注？
    private Date createTime;//创建时间
    private Date loginTime;//登录时间
    private Integer status;//权限状态，1表示启用，0表示禁用
    private static final long serialVersionUID = 1L;

    private Set<? extends GrantedAuthority> authorities;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.authorities;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.userName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        if(this.status==null){
            return false;
        }
        return this.status==1;
    }
}
