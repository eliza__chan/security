package com.eliza.security.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import java.util.Date;

@Data //自动设置方法
@AllArgsConstructor //有参构造器
@NoArgsConstructor//无参构造器
public class Permission implements GrantedAuthority {

    private Integer id;
    private Integer pid;
    private String name;
    private String value;
    private String icon;
    private Integer type;
    private String uri;
    private Integer status;
    private Date createTime;
    private Integer sort;


    @Override
    public String getAuthority() {
        return this.value;
    }
}
