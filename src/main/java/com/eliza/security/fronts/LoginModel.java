package com.eliza.security.fronts;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginModel implements Serializable {
    private String userName;
    private String password;
}
