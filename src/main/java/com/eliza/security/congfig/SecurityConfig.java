package com.eliza.security.congfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@EnableWebSecurity
//@EnableGlobalMethodSecurity(prePostEnabled = true)// 启用方法级别的权限认证
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    UserDetailService userDetailsService;

    @Autowired
    private JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter;

    @Autowired
    private RestAuthenticationEntryPoint restAuthenticationEntryPoint;

    @Autowired
    private RestfulAccessDeniedHandler restfulAccessDeniedHandler;

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf()
                .and()
                .cors().disable()
                .authorizeRequests()
                .antMatchers( "/user/login")
                .permitAll() //都可以访问
//                .antMatchers(HttpMethod.OPTIONS).permitAll()
//                .antMatchers("/users/**").hasRole("ADMIN") //需要相应的角色才能访问
                .anyRequest().authenticated() // 任何请求都需要认证
//                .formLogin() //基于Form表单登录验证
                .and()
                .sessionManagement()// 基于token，所以不需要 securityContext
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .userDetailsService(userDetailsService)
        ;

        //添加拦截器
        http.addFilterBefore(jwtAuthenticationTokenFilter, UsernamePasswordAuthenticationFilter.class);

        //添加自定义未授权和未登录结果返回
        http.exceptionHandling()
                .accessDeniedHandler(restfulAccessDeniedHandler)
                .authenticationEntryPoint(restAuthenticationEntryPoint);
    }
    //   这是将用户存入内存的第一种方式
//    @Autowired
//    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
//        auth.inMemoryAuthentication() //认证信息存储到内存中
//                .passwordEncoder(passwordEncoder())
//                .withUser("user").password(passwordEncoder().encode("123456")).roles("ADMIN")
//                .authorities("wx:product:read","wx:product:delete");//设置可读可删除权限
//    }

    //   这是将用户存入内存的第二种方式
//    @Override
//    public UserDetailsService userDetailsServiceBean() throws Exception {
//        InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
//        manager.createUser(new User("permission",passwordEncoder().encode("123456"),     AuthorityUtils.createAuthorityList("wx:product:read","wx:product:delete")));
//        return manager;
//    }

    //   这是密码加密解密器 可以用于加密密码也可以用户对比原始密码与加密密码，你只需暴露，security会将你暴露的passwordEncoder 作为默认的
    //   passwordEncoder.encode()  用于加密密码
    //   passwordEncoder.matchs(原始密码，加密密码)  对比原始密码与加密密码
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
