package com.eliza.security.congfig;

import com.eliza.security.model.Permission;
import com.eliza.security.model.UserModel;
import com.eliza.security.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;

@Component("userDetailsService")
public class UserDetailService implements UserDetailsService {

    @Autowired
    UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        UserModel userByName = userService.getUserByName(username);
        Integer id = userByName.getId();
        List<Permission> permissions = userService.getPermissionByUserId(id);
        HashSet hashSet = new HashSet(permissions);
        userByName.setAuthorities(hashSet);
        return userByName;
    }
}
