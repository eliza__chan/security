package com.eliza.security;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootTest
class SecurityApplicationTests {

    @Test
    void contextLoads() {
        BCryptPasswordEncoder bcryptPasswordEncoder = new BCryptPasswordEncoder();

        String hashPass = bcryptPasswordEncoder.encode("123456");
        System.out.println(hashPass);

//        String p1 = bcryptPasswordEncoder.

//        boolean b = bcryptPasswordEncoder.matches("123456","$10$NZ5o7r2E.ayT2ZoxgjlI.eJ6OEYqjH7INR/F.mXDbjZJi9HF0YCVG");
//        System.out.println(b);
    }

}
